package delivery.converter;

import delivery.model.Customer;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;


@Component
public class CustomerConverter implements Converter<String, Customer>
{
    @Override
    public Customer convert(String source)
    {
        try
        {
            Customer customer = new Customer(source);

            return customer;

        } catch (NumberFormatException ex)
        {
            System.out.println("ServerException.getServerException(ex).traceToSring()");

            return new Customer();
        }
    }
}
