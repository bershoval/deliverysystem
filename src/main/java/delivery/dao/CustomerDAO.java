package delivery.dao;

import delivery.mapper.CustomerRowMapper;
import delivery.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;
import java.util.Optional;

public class CustomerDAO
{
    private static final String GET = "SELECT PHONE FROM CUSTOMER WHERE PHONE = ?";
    private static final String GET_ALL = "SELECT PHONE FROM CUSTOMER";

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public CustomerDAO(JdbcTemplate template)
    {
        this.jdbcTemplate = template;
    }

    public Optional<Customer> get(String phone)
    {
        List<Customer> customers = jdbcTemplate.query
                (
                        GET
                        , ps -> ps.setString(1, phone)
                        , new CustomerRowMapper()
                );

        Optional<Customer> person = customers.stream().filter(x-> x.getPhone() == phone).findFirst();

        return person;
    }

    public List<Customer> getAll()
    {
        List<Customer> customers = null;
        customers = jdbcTemplate.query(GET_ALL, new CustomerRowMapper());
        System.out.println(customers);
        return customers;
    }

    // TODO
        //Список адресов клиента
        //Добавить адрес клиента
        //Изменить адрес клиента
}
